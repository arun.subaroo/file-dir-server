package main

import (
	"context"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"flag"
	"log"
	"math/big"
	"net"
	"net/http"
	"net/http/httptest"
	"os"
	"os/signal"
	"time"
)

type middleware func(http.Handler) http.Handler

func main() {
	var (
		port       = flag.String("p", "8080", "port to serve file on (default 8080)")
		dir        = flag.String("d", ".", "the directory to server (default .)")
		verbose    = flag.Bool("v", false, "verbosity of the log (default false)")
		secure     = flag.Bool("s", false, "run http or https default(false)")
		ecdsaCurve = flag.String("ecdsa-curve", "P256", "ECDSA curve to use to generate a key. Valid values are P224, P256 (recommended), P384, P521")

		done   = make(chan error, 1)
		curve  = elliptic.P256()
		logger = log.New(os.Stdout, "", 0)
	)
	flag.Parse()

	if *secure {
		switch *ecdsaCurve {
		case "P224":
			curve = elliptic.P224()
		case "P256":
			curve = elliptic.P256()
		case "P384":
			curve = elliptic.P384()
		case "P521":
			curve = elliptic.P521()
		default:
			log.Printf("Unrecognized elliptic curve: %q, using P256 by default...\n", *ecdsaCurve)
		}
	}

	hosts, tlsConfig, err := genCertsAndHosts(curve, *port, *secure)
	if err != nil {
		log.Printf("Unable to generate tlsConfig: %v\n", err)
	}

	srv := &http.Server{
		Addr:     ":" + *port,
		Handler:  logingMiddleware(log.New(os.Stdout, "", log.LstdFlags), *verbose)(http.FileServer(http.Dir(*dir))),
		ErrorLog: logger,
	}

	if *secure {
		srv.TLSConfig = tlsConfig
	}

	go func(d chan error, s *http.Server) {
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)
		<-c
		signal.Stop(c)
		close(c)
		if err := s.Shutdown(context.Background()); err != nil {
			logger.Printf("Error while shutting the server down:%v \n", err)
		}
		d <- err
	}(done, srv)

	logger.Printf("serving files from %s on %s\n", *dir, hosts)
	if *secure {
		if err := srv.ListenAndServeTLS("", ""); err != nil && err != http.ErrServerClosed {
			logger.Printf("Error while running server: %v\n", err)
		}
	} else {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			logger.Printf("Error while running server: %v\n", err)
		}
	}
	if err, ok := <-done; ok && err == nil {
		logger.Printf("Server has been shutdown successfully.\n")
	}
}

func logingMiddleware(logger *log.Logger, verbose bool) middleware {
	return middleware(func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			rec := httptest.NewRecorder()
			defer func(start time.Time, recorder *httptest.ResponseRecorder) {
				if verbose {
					logger.Printf("\nMethod:\t\t%v\nURL:\t\t%v\nAddr:\t\t%v\nUser Agent:\t%v\nRequest Time:\t%v\nResponse:\t%+v\n\n", r.Method, r.URL.Path, r.RemoteAddr, r.UserAgent(), time.Since(start), *rec.Result())
				} else {
					logger.Printf("%v -- \"%v %v %v\" %v %v -- in %v", r.RemoteAddr, r.Method, r.URL.Path, rec.Result().Proto, rec.Result().StatusCode, rec.Result().Status, time.Since(start))
				}
				for k, v := range rec.Header() {
					w.Header()[k] = v
				}
				w.WriteHeader(rec.Code)
				w.Write(rec.Body.Bytes())
			}(time.Now(), rec)
			h.ServeHTTP(rec, r)
		})
	})
}

func genCertsAndHosts(curve elliptic.Curve, port string, secure bool) (string, *tls.Config, error) {
	hosts := ""
	var template x509.Certificate

	iFaces, err := net.Interfaces()
	if err != nil {
		return hosts, &tls.Config{}, err
	}

	for _, iface := range iFaces {
		addrs, err := iface.Addrs()
		if err != nil {
			return hosts, &tls.Config{}, err
		}
		for _, v := range addrs {
			var ip net.IP
			switch val := v.(type) {
			case *net.IPNet:
				ip = val.IP
			case *net.IPAddr:
				ip = val.IP
			}

			if ip != nil && !ip.IsLoopback() {
				ip = ip.To4()
				if ip != nil {
					if secure {
						template.IPAddresses = append(template.IPAddresses, ip)
						hosts += "\n" + "https://" + ip.String() + ":" + port
					} else {
						hosts += "\n" + "http://" + ip.String() + ":" + port
					}
				}
			}
		}
	}

	if secure {
		priv, err := ecdsa.GenerateKey(curve, rand.Reader)
		if err != nil {
			return hosts, &tls.Config{}, err
		}

		serialNumber, err := rand.Int(rand.Reader, new(big.Int).Lsh(big.NewInt(1), 128))
		if err != nil {
			return hosts, &tls.Config{}, err
		}

		template.SerialNumber = serialNumber
		template.Subject = pkix.Name{Organization: []string{"Acme Co"}}
		template.NotBefore = time.Now()
		template.NotAfter = time.Now().Add(365 * 24 * time.Hour)
		template.KeyUsage = x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature
		template.ExtKeyUsage = []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth}
		template.BasicConstraintsValid = true

		derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, &priv.PublicKey, priv)
		if err != nil {
			return hosts, &tls.Config{}, err
		}

		cert, err := x509.ParseCertificate(derBytes)
		if err != nil {
			return hosts, &tls.Config{}, err
		}

		return hosts, &tls.Config{
			Certificates: []tls.Certificate{
				{
					Certificate: [][]byte{derBytes},
					PrivateKey:  priv,
					Leaf:        cert,
				},
			},
		}, nil
	}

	return hosts, &tls.Config{}, nil
}
